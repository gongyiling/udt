#include "utils.h"

long long htonll(long long v)	//assume the arch is little endian.
{
	long long ret;
	*(long*)&ret = htonl( *((long*)&v + 1) );
	*((long*)&ret + 1) = htonl( *((long*)&v) );
	return ret;
}

long long ntohll(long long v)
{
	return htonll(v);
}

