#pragma once

/*
	Author:	gongyiling@myhada.com
	Date:	2011-7-14
*/
#include <boost/asio/ip/basic_endpoint.hpp>
#include <boost/asio/ip/basic_resolver.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/host_name.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/thread/mutex.hpp>
#include "udt_log.h"

template <class InternetProtocol, class Endpoints>
size_t get_local_endpoints_v4(boost::asio::io_service& ios, const char* service, Endpoints& eps)
{
	eps.clear();
	boost::system::error_code ignored;
	boost::asio::ip::basic_resolver_query<InternetProtocol> query(boost::asio::ip::host_name(ignored), service);
	boost::asio::ip::basic_resolver<InternetProtocol> resolver(ios);
	boost::asio::ip::basic_resolver_iterator<InternetProtocol> i = resolver.resolve(query, ignored), end;
	while (i != end)
	{
		boost::asio::ip::basic_endpoint<InternetProtocol> ep = i->endpoint();
		if (ep.protocol() == InternetProtocol::v4() && 
			std::find(eps.begin(), eps.end(), ep) == eps.end())
		{
			LOG_VAR(ep);
			eps.push_back(ep);
		}
		++i;
	}
	return eps.size();
}

long long htonll(long long v);	//assume the arch is little endian.

long long ntohll(long long v);

class event
{
public:
	event():m_wait(false){}
	void signal()
	{
		m_wait = false;
		m_cond.notify_one();
	}
	void clear()
	{
		m_wait = true;
	}
	void wait()
	{
		boost::mutex::scoped_lock lock(m_mutex);
		while(m_wait)
		{
			m_cond.wait(lock);
		}
	}
private:
	boost::condition_variable m_cond;
	boost::mutex m_mutex;
	bool m_wait;
};

template <typename Handler>
struct invoker_wrapper
{
	invoker_wrapper(const Handler* h, event* e):handler(h), e(e){}
	void operator()()
	{
		(*handler)();
		e->signal();
	}
private:

	const Handler* handler;
	event* e;
};

template<typename Handler>
void invoke_io_service(boost::asio::io_service& ios, const Handler* handler)
{
	event e;
	ios.dispatch(invoker_wrapper<Handler>(handler, &e));
	e.wait();
}

template <typename Handler, typename ResultType>
struct invoker_result_wrapper
{
	invoker_result_wrapper(const Handler* h, ResultType* r, event* e):handler(h), result(r), e(e){}
	void operator()()
	{
		*result = (*handler)();
		e->signal();
	}
private:
	ResultType* result;
	const Handler* handler;
	event* e;
};

template<typename Handler, typename ResultType>
void invoke_io_service(boost::asio::io_service& ios, const Handler* handler, ResultType* result)
{
	event e;
	ios.dispatch(invoker_result_wrapper<Handler, ResultType>(handler, result, &e));
	e.wait();
}

template <typename Handler>
struct post_wrapper
{
	post_wrapper(const Handler& h, const boost::weak_ptr<void>& w):handler(h), wptr(w){}
	void operator()()
	{
		if (!wptr.expired())
		{
			handler();
		}
	}
private:
	Handler handler;
	boost::weak_ptr<void> wptr;
};

template<typename Handler>
void post_io_service(boost::asio::io_service& ios, const Handler& handler, const boost::weak_ptr<void>& wptr)
{
	ios.post(post_wrapper<Handler>(handler, wptr));
};
